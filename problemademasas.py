#Plano inclinado con fuerza de rozamiento.

# Para resolver este ejercicio,
# es necesario saber la definición de la segunda ley de Newton,
# conceptos de física y la formula para resolver el sistema de masas. Además de 
# los conceptos vistors a lo largo de la asignatura de POO.

import math as m

class Masas:
    
     def calcular_movimiento(self,angulo,cr,m1):
        #La ecuación es: F=m2*g-(m1*g*(sin(a)+u*cos(a)))
        #Si F>0 es porque el sistema está bajando, de lo contrario el sistema está en equilibrio o está subiendo.
        lista=[]
        m2=0
        aux=-m1*9.8*(m.sin(m.radians(angulo))+cr*m.cos(m.radians(angulo)))
        lista.append([m2,'subiendo'])
        while aux<0:
            m2+=0.5
            aux=m2*9.8-(m1*9.8*(m.sin(m.radians(angulo))+cr*m.cos(m.radians(angulo))))
            lista.append([m2,'subiendo'])
        lista.append([m2,'bajando'])
        return lista
    
     def calcular_angulo(self,m1,m2,cr):
        #La ecuación es: F=m2*g-(m1*g*(sin(a)+u*cos(a)))
        #Si F=0 es porque el sistema está en equilibrio.
        resp=-1
        aux=0
        for i in range(75):
            aux=round(m2*9.8-(m1*9.8*(m.sin(m.radians(i+10))+cr*m.cos(m.radians(i+10)))),1)
            if aux==0:
                resp=i+10
                break
        return resp 
    
a=Masas()

print(a.calcular_movimiento(55,0.15,6))
print(a.calcular_angulo(8,6,0.2))
